package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;

public class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final String userId = getUserId();
        getProjectService().removeAll(userId);
    }

}
