package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show program version.";
    }

}
