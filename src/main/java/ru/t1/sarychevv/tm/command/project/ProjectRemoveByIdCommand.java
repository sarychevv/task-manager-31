package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, id);
    }

}
