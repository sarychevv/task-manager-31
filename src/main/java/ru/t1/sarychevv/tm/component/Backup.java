package ru.t1.sarychevv.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.command.data.AbstractDataCommand;
import ru.t1.sarychevv.tm.command.data.DataBackupLoadCommand;
import ru.t1.sarychevv.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup extends Thread {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void close() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

}
